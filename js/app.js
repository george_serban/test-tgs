(function() {
    var app = new Vue({
        el: "#app",
        data: {

            websiteTitle: "Social Media App", 
            restUrl: "https://reqres.in/api/users",
            page: 0, //initial value 0, updated after ajax
            total_users: 0, //initial value 0, updated after ajax
            total_pages: 0, //initial value 0, updated after ajax
            per_page: 0, //initial value 0, updated after ajax
            users: [] //initial empty array, updated after ajax
        },
        methods: {
            fetchData: function() { 
                //ajax request to the rest service

                var $this = this;

                axios.get( $this.restUrl + $this.restUrlParams() ).then(function(response) {

                    //if success, populate data elements with info from ajax

                    $this.page = response.data.page;
                    $this.total_pages = response.data.total_pages;
                    $this.per_page = response.data.per_page;
                    $this.total_users = response.data.total;
                    $this.users = response.data.data;

                }).catch(function(error) {
                    //if ajax error, error handler
                    console.log(error);
                });
            
            },
            
            restUrlParams: function(){
                //the params we wish to add to restUrl, after user changes the pagination or friends per page
                
                var params = '';
                if( this.page != 0 ){
                    params = '?per_page=' + this.per_page + '&page=' + this.page;
                }
                return params;
            },

            changeUsersPerPage: function(){
                //user selects the number of friends per page; reset the page number so we dont mess up the query and fetch data
                this.page = 1;
                this.fetchData()
            },
            
            changePage: function( pageNumber ){
                //change friends list from pagination buttons
                this.page = pageNumber;
                this.fetchData();
            }
        },
        mounted: function() {
            this.fetchData();
        }
    });
})();